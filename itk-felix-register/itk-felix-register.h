#ifndef __FLX_REGISTER_HANDLER__
#define __FLX_REGISTER_HANDLER__

#include <string>
#include <vector>
#include <cstdint>
#include <felix_proxy/ClientThread.h>

class FLXRegisterHandler
{
  public:

    FLXRegisterHandler() = delete;

    FLXRegisterHandler(
	      std::string interface,	      
	      std::string buspath,
              uint64_t fID_CMD,
	      bool resubscribeValue = false);
    ~FLXRegisterHandler();

    void setMaxRetries(int);
    void Connect();
    void Disconnect();
    void sendRegs(std::map<std::string, uint64_t>);
    void sendTrigger();
    void sendECR();
    void sendBCR();

    std::vector<std::pair<std::string, std::vector<uint8_t>>> readRegs(std::vector<std::string>);

  private:
    
    /**
     * Carry out the actual netio communication.
     * Expect a reply for every netio fragment.
     * Check the rx every waitmillis, for a total of maxattempts.
     * If resubscribeonretry is set, the netio communication will be closed and opened again before every new attempt.
     * @param Cmd to send (SET, GET)
     * @param Registers to SET/GET
     * @param Value to SET (omitted in GET)
     * @param Duration to wait between communications
     * @return true if commnunication was possible 
     */
    bool communicate(felix_proxy::ClientThread::Cmd cmd, std::string REGISTER, uint64_t Value, uint32_t waitmillis=5);

    felix_proxy::ClientThread * m_NextClient;
    felix_proxy::ClientThread::Config  m_NextConfig;

    std::vector<uint8_t> m_reply;
    
    int m_maxRetries;
    bool m_resubscribe;
    uint64_t m_fID_CMD;
    bool m_verbose;
    std::string m_interface;
    std::string m_buspath;

  protected:


};


#endif
