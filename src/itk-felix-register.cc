#include "itk-felix-register/itk-felix-register.h"
#include <chrono>
#include <exception>
#include <fstream>

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"

struct FelixCmdHeader{
  uint32_t length;
  uint32_t reserved;
  uint64_t elink;
};

using namespace std;


namespace py = pybind11;
PYBIND11_MODULE(libitk_felix_register, m) {
  py::class_<FLXRegisterHandler> (m, "FLXRegisterHandler")
    .def(py::init<std::string, std::string, const uint64_t,const bool>())
    .def("setMaxRetries", (&FLXRegisterHandler::setMaxRetries),"Set Max Retries for writting")
    .def("Connect", (&FLXRegisterHandler::Connect),"Connect to the felix-register")
    .def("sendRegs", (&FLXRegisterHandler::sendRegs),"Send the following registers")
    .def("readRegs", (&FLXRegisterHandler::readRegs),"Read the given set of registers", py::return_value_policy::copy)
    .def("sendTrigger", (&FLXRegisterHandler::sendTrigger),"Send a single L1A to the device", py::return_value_policy::copy)
    .def("sendECR", (&FLXRegisterHandler::sendECR),"Send a single ECR to the device", py::return_value_policy::copy)
    .def("sendBCR", (&FLXRegisterHandler::sendBCR),"Send a single BCR to the device", py::return_value_policy::copy)
;
}






FLXRegisterHandler::FLXRegisterHandler(
		     const std::string interface,
		     const std::string buspath,
                     const uint64_t fID_CMD,
                     const bool resubscribeValue
                     ){
  m_maxRetries = 2;
  m_resubscribe=resubscribeValue;
  m_fID_CMD=fID_CMD;
  m_interface=interface;
  m_buspath=buspath;
  m_verbose=false;

  std::cout<<"Handler::Connect Interface to be used is:"<<m_interface<<endl;
  std::cout<<"Handler::Connect bus dir   to be used is:"<<m_buspath<<endl; 

  m_NextConfig.property["local_ip_or_interface"] = m_interface;//"127.0.0.1";
  
  if(m_verbose) // Error level is defined by verbosity level
    m_NextConfig.property["log_level"] = "trace";
  else
     m_NextConfig.property["log_level"] = "error";

  m_NextConfig.property["bus_dir"] = m_buspath;
  m_NextConfig.property["bus_group_name"] = "FELIX";
  // m_NextConfig.property["verbose_bus"] = "True";
  // m_NextConfig.property["netio_pages"] = "256";
  // m_NextConfig.property["timeout"] = "2";
  // m_NextConfig.property["netio_pagesize"] = "";
  // m_NextConfig.property["thread_affinity"] = "";

  m_NextConfig.on_init_callback = []() {
    std::cout<<"Handler::ClientThread::Initialiasing connection"<<endl;
  };

  m_NextConfig.on_connect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Connecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

    // if (fid == subscribe_tag) {
    //   subscribed = true;
    // }

  };

  m_NextConfig.on_disconnect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Disconnecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };




  //Shouldn't handle service frames yet
  m_NextConfig.on_data_callback =  [&](const std::uint64_t rx_elink,
						   const std::uint8_t* data,
						   const std::size_t size,
						   const std::uint8_t status){
  };

  m_NextClient = new felix_proxy::ClientThread(m_NextConfig);

}






void FLXRegisterHandler::Connect()
{
  m_NextClient->subscribe(m_fID_CMD);
}


void FLXRegisterHandler::Disconnect()
{
  m_NextClient->unsubscribe(m_fID_CMD);

}




FLXRegisterHandler::~FLXRegisterHandler()
{
  delete m_NextClient;
}


void FLXRegisterHandler::setMaxRetries(int val) {
  if(val >= 0)
    m_maxRetries = val;
  else
    m_maxRetries = 2;
}


void FLXRegisterHandler::sendTrigger() {
  
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_L1A",0);
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_L1A",1);
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_L1A",0);

}


void FLXRegisterHandler::sendECR() {
  
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_ECR",0);
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_ECR",1);
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_ECR",0);

}

void FLXRegisterHandler::sendBCR() {
  
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_BCR",0);
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_BCR",1);
  communicate(felix_proxy::ClientThread::Cmd::SET,"TTC_EMU_CONTROL_BCR",0);

}





void FLXRegisterHandler::sendRegs(std::map<std::string, uint64_t> regs) {
  
  for(auto& reg : regs) {
    communicate(felix_proxy::ClientThread::Cmd::SET,reg.first,reg.second);
  }
}


std::vector<std::pair<std::string, std::vector<uint8_t>>> FLXRegisterHandler::readRegs(std::vector<std::string> regs) {

  std::vector<std::pair<std::string, std::vector<uint8_t > > > retVal;
  for (const auto& reg : regs) {
    communicate(felix_proxy::ClientThread::Cmd::GET,reg,0);
    if (m_reply.size() > 0) {
      retVal.emplace_back(reg, m_reply);
    }
  }
  return retVal;
}



bool FLXRegisterHandler::communicate(felix_proxy::ClientThread::Cmd cmd, std::string REGISTER, uint64_t Value,uint32_t waitmillis){

  uint32_t attempt=0;
  while(true){
    std::vector<std::string> cmd_vec;
    if (cmd==felix_proxy::ClientThread::Cmd::SET){
      cmd_vec.push_back(REGISTER);
      cmd_vec.push_back(std::to_string(Value));
    }
    else if (cmd==felix_proxy::ClientThread::Cmd::GET){
      cmd_vec.push_back(REGISTER);

    }
    
    ///FelixClientThreadExtension::Cmd::TRIGGER

    //std::cout<<"Will try with 0x"<<std::hex<<m_fID_CMD<<std::dec<<std::endl;
    std::vector<uint64_t> elinks = {m_fID_CMD};
    std::vector<felix_proxy::ClientThread::Reply> replies;
    m_NextClient->send_cmd(elinks, cmd, cmd_vec,replies);
    std::this_thread::sleep_for(std::chrono::milliseconds(waitmillis));
    m_reply.clear();
    for(auto const& reply : replies) {
      m_reply.push_back(reply.value);
    }
    if(m_reply.size()) break;
    attempt++;
    if(attempt>(uint32_t)m_maxRetries){return false;}
    
    if(m_resubscribe){
      m_NextClient->unsubscribe(m_fID_CMD);
      m_NextClient->subscribe(m_fID_CMD);
    }



  }
  return true;
}
