
# Introduction
itk-felix-register is a tool is a mini-software library that was designed for FELIX hardware, that allows change register commands to be sent over felixstar to felix-hardware. 
The software is written in C++ and can be used as a c++ class or a python module. 


# Compile Instructions 
The package dependencies are kept at a minimum only PYBIND11 and TBB packages are required
For machines with CVMFS acceses you should be able to compile this package out of the box following these instructions

```
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-felix-sw.git
cd itk-felix-sw
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-felix-register.git
source setup.sh
mkdir build
cd build
cmake ../
make -j install
```

For non CVMFS machines, either TDAQ package can be installed or a custom CMAKE file could be written as the dependencies are super small.  

# How to use
To use the software you just need to source the itk-felix-sw by calling
```
cd itk-felix-sw
source setup.sh
```

Then you can checkout the use example on the ic-over-netio/test.py example. 
Main idea is you connect to a given Felix server in a given channel using (Python): 
```
from libitk_felix_register_test import *
IC = FLXRegisterHandler("lo", "/tmp/bus/", 0x1000000812008000, False)
#REG="TTC_EMU_CONTROL_L1A"
REG="TTC_DEC_CTRL_MASTER_BUSY"
print("---------- HERE ----",IC.readRegs([REG]))
IC.sendRegs({REG:0x1})
print("---------- HERE ----",IC.readRegs([REG]))
IC.sendRegs({REG:0x0})
print("---------- HERE ----",IC.readRegs([REG]))
```
where the individual fields are:
```
ICNetioNextHandler(<Network Interface>,
	  <BUS published by NetioNext>, 
	  <CMD FID>, 
	  <Attempt to re-subscribe if communication fails>,
```

Then individual IC commands can be sent to

1. read registers
```
IC.readRegs([<"ARRAY of REGISTER NAMES">])
```
2. write registers
```
IC.sendRegs({ <REGISTERNAME>:<Value to write> })
```